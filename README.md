# Comet Clients #

This repository contains OpenAPI clients in Java and python to interact with the Comet prioritization service. For each language there is also an example on how to use the clients.

## Comet Overview ##

![comet workflow](comet-overview.png)

Comet is a test case prioritization service that integrates with CI/CD platforms. Users shall send data related to the tested software project and the test cases that are planned for execution. Comet then returns the list of test cases reordered so that the test cases that have the highest probability to fail have been put first.

Formally speaking, users must send data to comet at two stages of the CI/CD process: 

* Before test execution: The test cases marked for execution. Comet returns the prioritized execution order.  
* After test execution: The test execution results.

Note that users are responsible for executing the test cases in the order prescribed by Comet.

## API Clients ##

We provide usage examples with API clients in [Java](java/comet-javaclient/README.md) and [python](python/comet_pycli/README.md). Both rely on the deeplearning4j dataset. They simulate multiple CI/CD cycles by looping through the test cycles in the dataset, and for each cycle, comunicating with Comet as if it was a live CI/CD cycle. The following subsections explain how to run the examples. Note that you need to configure the code examples with the correct URL of Comet and your authentication key.

### Java ###

You need Java 11+ and Maven installed.

First, the client must be packaged and put in your local repository. Go to java/comet-javaclient and run:
```console
foo@bar:~/comet-clients/java/comet-javaclient$ mvn clean install
```
Second, copy the dataset to the resource folder of the comet-javaclient-example project:
```console
foo@bar:~/comet-clients$ cp dataset/deeplearning4j-api.csv java/comet-javaclient-example/src/main/resources/com/smartesting/comet
```
Third, go to java/comet-javaclient-example and run:
```console
foo@bar:~/comet-clients/java/comet-javaclient-example$ mvn compile exec:java -Dexec.mainClass="com.smartesting.comet.CometJavaClientExample"
```
The program should output a CSV file containing the APFD scores of Comet and the prioritization times.

### python ###

You need Conda installed. 

First, let's create the conda environment and activate it. In the python/ directory, run:
```console
(base) foo@bar:~/comet-clients/python$ conda env create -f conda-env.yml
(cometclient) foo@bar:~/comet-clients/python$ conda activate cometclient
```
Then, to execute the example, run the following command in the python directory:
```console
(cometclient) foo@bar:~/comet-clients/python$ python api_usage_example_dl4j.py
```
Similarly, the program should generate a CSV file with APFD scores.

## Comet API Usage ##

This section describes the Comet API workflow i.e., how clients should interact with the API in order to trigger the prioritization. 
The description is illutrated using one of Eclipse's projects, [deeplearning4j](https://github.com/eclipse/deeplearning4j), for which we have a large test execution history (1038 test cycles for ~15000 tests).

In Comet the data is organized in projects, which contain test cycles, which themselves contain test cases.
Test cycles, like test cases, have their own data that must be provided by the user to ensure the best 
possible prioritization.

Test cycles are characterized by the following *features*:

* **id** - *text*: (Required) Identifier of the test cycle.
* **sinceLast** - *int*: Duration in seconds since the last test cycle.
* **filesChanged** - *int*: Number of files changed in the project since the last test cycle.
* **insertions** - *int*: Number of lines of code added to the project since the last test cycle.
* **deletions** - *int*: Number of lines of code deleted from the project since the last test cycle.

The test cases are characterized by its ID and two optionnals *features*, the result and the duration:

* **id** - *text*: (Required) Test case identifier.
* **fail** - *boolean*: Execution result of the test case, true in case of failure, false otherwise.
* **duration** - *decimal*: Execution duration of the test in seconds.

> Since the **duration** and **fail** test case *features* can only be obtained after the test has been run, a specific API call is required to modify this information in a test already provided to Comet.
If your context allows you to provide the execution time of the test, it's **mandatory** to use the duration feature instead of an additional feature with a different name (e.g. execTime, timeExec, etc.).

You can provides additional *features* of your choice. For example, the CSV file we use contains the following: 

* **methodsNumber** - *integer*: Number of tests or methods if the provided test cases are test classes.
* **classChanged** - *boolean*: Indicates if the tested class has been modified since the last test cycle.
* **testChanged** - *boolean*: Indicates if the test case itself has been modified since the last test cycle.

### Creating the project

The first step is to create a prioritization project (one project = one SUT) in Comet via its name and to declare the additional features we planned to use.
An additional feature is defined by a name, a type (int, bool or float) and a defaultValue, all these fileds are **mandatory** to define a feature. It is possible to not use additional features.

#### POST /projects

The project attributes are provided in the **body** of the request:

```json
{
  "name": "deeplearning4j",
  features:  [
  {
    name: "methodsNumber",
    type: "int",
    defaultValue: "0"
  }, 
  {
    name: "classChanged",
    type: "bool",
    defaultValue: "false"
  },
  {
    name: "testChanged",
    type: "bool",
    defaultValue: "false"
  }]
}
```

This call is only made once, here only the name of the project was provided but if a test execution history exists,
it is possible to provide it in full when creating the project.

```json
{
  "name": "deeplearning4j",
  "testCycles": [
    {
      "id": "19017690",
      "sinceLast": 0,
      "filesChanged": 3,
      "insertions": 234,
      "deletions": 3,
      "tests": [
        {
          "id": "com.ccc.deeplearning.rbm.matrix.jblas.CRBMTest",
          "duration": "3.296",
          "methodsNumber": 2,
          "classChanged": false,
          "testChanged": false,
          "fail": true
        },
        {
          "id": "com.ccc.deeplearning.rbm.matrix.jblas.RBMTest",
          "duration": "0.621",
          "methodsNumber": 2,
          "classChanged": false,
          "testChanged": false,
          "fail": true
        },
        ...
      ]
    },
    {
      "id": "19096813",
      ...
    }
  ]
}
```

The name given to the project serves as a reference and is reused in all subsequent calls.

### Creating a test cycle

Each new test cycle must be declared. As with the creation of a project, it is possible to
declare the test cycle with a few basic attributes and then add tests afterwards:

#### POST /projects/deeplearning4j/testCycles

The attributes of the test cycle are provided in the **body** of the request, namely commit-related (code change) information and the number of seconds elapsed since the last cycle.

```json
{
  "id": "19017690",
  "sinceLast": 3600,
  "filesChanged": 3,
  "insertions": 234,
  "deletions": 3
}
```

> The attributes of the cycle are optional, only the cycle identifier is required. Now that the cycle is declared in comet, test cases can be added to it.

### Adding tests to a cycle

The cycle **19017690** has been declared to the API, so there are two possibilities for declaring the tests that will be
executed during this cycle. Either the entire test suite is added in a single call:

#### PUT /projects/deeplearning4j/testCycles/19017690/suite

**body** :

```json
[
  {
    "id": "org.deeplearning4j.iterativereduce.tracker.statetracker.zookeeper.ZooKeeperStateTrackerTest",
    "classChanged": false,
    "testChanged": false
  },
  {
    "id": "org.deeplearning4j.nn.HiddenLayerTest",
    "classChanged": false,
    "testChanged": true
  },
  ...
  {
    "id": "org.deeplearning4j.zookeeper.TestZookeeperRegister",
    "classChanged": true,
    "testChanged": false
  }
]
```

> Careful! This call replaces the existing tests of this cycle if there are any.

Another possibility is to iterate on the tests and add them to the test cycle one by one:

#### POST /projects/deeplearning4j/testCycles/19017690/test

**body**:

```json
{
  "id": "org.deeplearning4j.nn.HiddenLayerTest",
  "classChanged": false,
  "testChanged": true
}
```

#### POST /projects/deeplearning4j/testCycles/19017690/test

**body**:

```json
{
  "id": "org.deeplearning4j.iterativereduce.tracker.statetracker.zookeeper.ZooKeeperStateTrackerTest",
  "classChanged": false,
  "testChanged": false
}
```

etc.
> As for the cycles, the *classChanged* and *testChanged* test attributes are optional. These are boolean attributes that inform whether the test code (testChanged) and tested code (classChanged) have been modified since the last test execution.

### Prioritization request

The tests of the **19017690** cycle being declared, it is possible to make a prioritization request on this test cycle. 
Here again, there are several ways to submit a prioritization request.

In this case, the simplest way is to submit a prioritization request directly on the cycle
**19017690** via its identifier:

#### GET /projects/deeplearning4j/testCycles/19017690/tcp

The API then returns the list of tests of the cycle ordered according to the execution order suggested by the
prioritization algorithm.

```json
[
  "org.deeplearning4j.models.word2vec.WordVectorSerializerTest",
  "org.deeplearning4j.models.layers.ConvolutionDownSampleLayerTest",
  "org.deeplearning4j.clustering.vptree.VpTreeNodeTest",
  "org.deeplearning4j.scaleout.perform.NeuralNetWorkPerformerTest",
  "org.deeplearning4j.plot.BarnesHutTsneTest",
  "org.deeplearning4j.eval.EvalTest",
  "org.deeplearning4j.iterativereduce.tracker.statetracker.zookeeper.ZooKeeperStateTrackerTest",
  "org.deeplearning4j.scaleout.perform.text.WordCountTest",
  "org.deeplearning4j.clustering.quadtree.QuadTreeTest",
  "org.deeplearning4j.util.TimeSeriesUtilsTest",
  "org.deeplearning4j.util.MovingWindowMatrixTest",
  "org.deeplearning4j.nn.HiddenLayerTest",
  "org.deeplearning4j.util.ArrayUtilTest",
  "org.deeplearning4j.util.EnumUtilTest",
  "org.deeplearning4j.nn.learning.AdaGradTest",
  "org.deeplearning4j.zookeeper.TestZookeeperRegister",
  "org.deeplearning4j.distributions.DistributionsTest",
  "org.deeplearning4j.datasets.DataSetTest"
]
```
> To perform prioritization via the test cycle identifier, it is necessary to ensure that tests are present in the test cycle.

To reduce the number of api calls, it is possible to provide the test suite at the same time as the prioritization request. This case the following entry point is used:

#### POST /projects/deeplearning4j/testCycles/19017690/tcp

**body**:
```json
[
  {
    "id": "org.deeplearning4j.iterativereduce.tracker.statetracker.zookeeper.ZooKeeperStateTrackerTest",
    "classChanged": false,
    "testChanged": false
  },
  {
    "id": "org.deeplearning4j.nn.HiddenLayerTest",
    "classChanged": false,
    "testChanged": true
  },
  ...
  {
    "id": "org.deeplearning4j.zookeeper.TestZookeeperRegister",
    "classChanged": true,
    "testChanged": false
  }
]
```
Finally, a third method exists to perform prioritization. It is useful in some very specific cases and
consists in providing a complete test cycle when requesting prioritization. This test cycle is considered as **temporary** and is not added to the database:

#### POST /projects/{name}/testCycles/tcp

**body**:

```json
{
  "sinceLast": 0,
  "filesChanged": 3,
  "insertions": 234,
  "deletions": 3,
  "tests": [
    {
      "id": "com.ccc.deeplearning.rbm.matrix.jblas.CRBMTest",
      "classChanged": false,
      "testChanged": false
    },
    {
      "id": "com.ccc.deeplearning.rbm.matrix.jblas.RBMTest",
      "classChanged": false,
      "testChanged": false
    },
    ...
   ]
}
```
> No test cycle identifier isn't provided here, since it is not added to the database it is not intended to be retrieved via its identifier afterwards.

### Sending the test execution results

Now that the test suite has been prioritized, it must be executed with the test framework used for the development of the project.

After execution and to keep the database on which the prioritization is based up to date, it is necessary to inform Comet of the test execution results. For each of the executed tests, three pieces of information must be sent via an API call:

#### PATCH /projects/deeplearning4j/testCycles/19017690/suite

**body**:

```json
[
  {
    "id": "org.deeplearning4j.iterativereduce.tracker.statetracker.zookeeper.ZooKeeperStateTrackerTest",
    "duration": "3.296",
    "methodNumber": 2,
    "fail": true
  },
  {
    "id": "org.deeplearning4j.nn.HiddenLayerTest",
    "duration": "0.621",
    "methodNumber": 2,
    "fail": true
  },
  {
    "id": "org.deeplearning4j.zookeeper.TestZookeeperRegister",
    "duration": "1.098",
    "methodNumber": 1,
    "fail": true
  }
]
```

Thus, the database is kept up to date and the prioritization algorithm can rely on the previous verdicts
to propose a better prioritization.
