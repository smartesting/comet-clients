# -*- coding: utf-8 -*-
from __future__ import print_function

import pandas as pd
import sys
import comet_pycli
from comet_pycli.rest import ApiException
from comet_pycli.exceptions import ServiceException
from timeit import default_timer as timer

def compute_APFD(test_cases: pd.DataFrame,ordered_ts: list = None) -> float:
    N: float = len(test_cases)
    M: float = len(test_cases[test_cases['verdict'] == 1])
    if ordered_ts == None:
        ordered_ts = test_cases['testcase_name'].values.tolist()
    if N > 0 and M > 0:
        apfd = 1 - (_sum_ranks(test_cases,ordered_ts) / (N * M)) + (1 / (2 * N))
    else:
        apfd = -1
    return apfd

def _sum_ranks(test_cases: pd.DataFrame, ordered_ts: list) -> int:
    sum_ranks: float = 0
    i: int = 1
    for tc in ordered_ts:
        sum_ranks = sum_ranks + test_cases[test_cases['testcase_name'] == tc]['verdict'].squeeze() * i
        i += 1
    return sum_ranks

def compute_optimal_APFD(ts_size, nbr_failed_tc):
    N: float = ts_size
    M: float = nbr_failed_tc
    if N > 0 and M > 0:
        return 1 - (sum(range(M+1)) / (N * M)) + (1 / (2 * N))
    else:
        return -1

#%% Configure API Client and load dataset

configuration = comet_pycli.Configuration(
    host= ''
)
configuration.api_key['ApiKeyAuth'] = ''

#%% Load data
prj = 'deeplearning4j'
team = prj
comet_prj = f"{prj}-demo"
base_path = f"../dataset/"
path = f'{base_path}{prj}-api.csv'
dataset = pd.read_csv(path,index_col=0)

#%% Init API
with comet_pycli.ApiClient(configuration) as api_client:
    test_cycles_api = comet_pycli.TestCyclesApi(api_client)
    projects_api = comet_pycli.ProjectsApi(api_client)
    tests_api = comet_pycli.TestsApi(api_client)
    prio_api = comet_pycli.PrioritizationsApi(api_client)
    
#%% Delete project if it already exists
    try:
        response = projects_api.delete_project(comet_prj)
        print(response)
    except ApiException as e:
        print(e)
        print("The project does not exist yet.")
    
#%% Create project
    try:
  #      projects_api.delete_project({'name': comet_prj})
        response = projects_api.create_project({'name': comet_prj})
        print(response)
    except ApiException as e:
        print(e)

#%% Simulating multiple CI cycles
    testcycle_ids, optimal_apfd, initial_apfd, comet_apfd, exec_times = [],[],[],[],[]
    current_cycle = 1
    nbr_cycles = len(dataset['job_id'].drop_duplicates())
    for test_cycle_id,test_cycle in dataset.groupby('job_id'):
        if test_cycle_id > 100:
            break;
        try:
            print(f"\r[INFO] Creating Test Cycle {current_cycle}/{nbr_cycles}")
            response = test_cycles_api.add_test_cycle(
                     comet_prj,
                     {
                         'id': test_cycle_id,
                     })
            nbr_testcases = len(test_cycle)
            nbr_failed_tc = test_cycle['verdict'].sum()
            # The first 20 test cycles are considered historical data and are pushed as is in DB
            if current_cycle < 30:
                print("\r[INFO] No prediction. Database storage only.")
                current_tc = 1
                for idx,testcase in test_cycle.iterrows():
                    
                    print(f"\r[INFO] Adding test {current_tc}/{len(test_cycle)}: {testcase['testcase_name']}")
                    response = tests_api.add_test(comet_prj, test_cycle_id,
                                              {
                                                  'id': testcase['testcase_name'],
                                                  'duration': testcase['duration'],
                                                  'fail': testcase['verdict'],
                                                  'classChanged': testcase['class_changed'],
                                                  'testChanged': testcase['test_changed']
                                              })
                    current_tc += 1
            # For each subsequent cycles:
            #    1. test data is sent to Comet
            #    2. Comet sends a prioritized order
            #    3. test execution results are sent to comet
            else:
                print("\r[INFO] Submitting test cases planned for execution.")
                current_tc = 1
                for idx,testcase in test_cycle.iterrows():
                    print(f"\r[INFO] Adding test {current_tc}/{len(test_cycle)}: {testcase['testcase_name']}")
                    response = tests_api.add_test(comet_prj, test_cycle_id,
                                              {
                                                  'id': testcase['testcase_name'],
                                                  'classChanged': testcase['class_changed'],
                                                  'testChanged': testcase['test_changed']
                                              })
                    current_tc += 1                           
                
                # Because we know in advance which test cycles do not contain failed test cases
                # It is useless to call comet on these
                if nbr_failed_tc > 0 and nbr_testcases > 6:
                    print(f"\r[INFO] Requesting prioritization...")
                    start = timer()
                    response = prio_api.prioritize(comet_prj,test_cycle_id)
                    prioritized_order = response.tests     
                    end = timer()
                    
                    # APFD scores are measured to evaluate Comet w.r.t the initial ordering
                    optimal_apfd.append(compute_optimal_APFD(len(test_cycle),nbr_failed_tc))
                    initial_apfd.append(compute_APFD(test_cycle))
                    comet_apfd.append(compute_APFD(test_cycle,prioritized_order))
                    exec_times.append(end-start)
                    testcycle_ids.append(test_cycle_id)
                    print(f"\r[INFO] {nbr_failed_tc}/{len(test_cycle)} test cases failed.")
                    print(f"\r[INFO] Prioritization took {(end-start):.2f} seconds.")
                    print(f"\r[INFO] APFD of Optimal order, initial order, prioritized order: {optimal_apfd[-1]}, {initial_apfd[-1]}, {comet_apfd[-1]}")
                else:
                    print(f"\r[INFO] All test cases passed. No analysis.")

                # Execute test cases in prioritized order and push execution results (test verdict and exec duration)
                response = tests_api.update_suite(comet_prj, test_cycle_id,
                                          [        
                                          {
                                              'id': testcase['testcase_name'],
                                              'duration': testcase['duration'],
                                              'fail': testcase['verdict']
                                          } for idx,testcase in test_cycle.iterrows()])
                
                    
        except ServiceException as se:
            print(se)
        except ApiException as ae:
            print(ae)
            sys.exit(1)
        current_cycle += 1
        
    comet_apfd_results = pd.DataFrame({'optimal_apfd': optimal_apfd, 
                                       'stock_apfd': initial_apfd,
                                       'comet_apfd': comet_apfd,
                                       'exec_time': exec_times},index=testcycle_ids)   
    comet_apfd_results.to_csv(f'{prj}-apiresults.csv')



