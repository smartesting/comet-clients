# coding: utf-8

"""
    Comet API

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)  # noqa: E501

    The version of the OpenAPI document: 0.0.1
    Generated by: https://openapi-generator.tech
"""


try:
    from inspect import getfullargspec
except ImportError:
    from inspect import getargspec as getfullargspec
import pprint
import re  # noqa: F401
import six

from comet_pycli.configuration import Configuration


class TestCycle(object):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """

    """
    Attributes:
      openapi_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    openapi_types = {
        'id': 'str',
        'tests': 'list[Test]',
        'since_last': 'int',
        'files_changed': 'int',
        'insertions': 'int',
        'deletions': 'int'
    }

    attribute_map = {
        'id': 'id',
        'tests': 'tests',
        'since_last': 'sinceLast',
        'files_changed': 'filesChanged',
        'insertions': 'insertions',
        'deletions': 'deletions'
    }

    def __init__(self, id=None, tests=[], since_last=0, files_changed=0, insertions=0, deletions=0, local_vars_configuration=None):  # noqa: E501
        """TestCycle - a model defined in OpenAPI"""  # noqa: E501
        if local_vars_configuration is None:
            local_vars_configuration = Configuration.get_default_copy()
        self.local_vars_configuration = local_vars_configuration

        self._id = None
        self._tests = None
        self._since_last = None
        self._files_changed = None
        self._insertions = None
        self._deletions = None
        self.discriminator = None

        self.id = id
        if tests is not None:
            self.tests = tests
        if since_last is not None:
            self.since_last = since_last
        if files_changed is not None:
            self.files_changed = files_changed
        if insertions is not None:
            self.insertions = insertions
        if deletions is not None:
            self.deletions = deletions

    @property
    def id(self):
        """Gets the id of this TestCycle.  # noqa: E501


        :return: The id of this TestCycle.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this TestCycle.


        :param id: The id of this TestCycle.  # noqa: E501
        :type id: str
        """
        if self.local_vars_configuration.client_side_validation and id is None:  # noqa: E501
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def tests(self):
        """Gets the tests of this TestCycle.  # noqa: E501


        :return: The tests of this TestCycle.  # noqa: E501
        :rtype: list[Test]
        """
        return self._tests

    @tests.setter
    def tests(self, tests):
        """Sets the tests of this TestCycle.


        :param tests: The tests of this TestCycle.  # noqa: E501
        :type tests: list[Test]
        """

        self._tests = tests

    @property
    def since_last(self):
        """Gets the since_last of this TestCycle.  # noqa: E501


        :return: The since_last of this TestCycle.  # noqa: E501
        :rtype: int
        """
        return self._since_last

    @since_last.setter
    def since_last(self, since_last):
        """Sets the since_last of this TestCycle.


        :param since_last: The since_last of this TestCycle.  # noqa: E501
        :type since_last: int
        """

        self._since_last = since_last

    @property
    def files_changed(self):
        """Gets the files_changed of this TestCycle.  # noqa: E501


        :return: The files_changed of this TestCycle.  # noqa: E501
        :rtype: int
        """
        return self._files_changed

    @files_changed.setter
    def files_changed(self, files_changed):
        """Sets the files_changed of this TestCycle.


        :param files_changed: The files_changed of this TestCycle.  # noqa: E501
        :type files_changed: int
        """

        self._files_changed = files_changed

    @property
    def insertions(self):
        """Gets the insertions of this TestCycle.  # noqa: E501


        :return: The insertions of this TestCycle.  # noqa: E501
        :rtype: int
        """
        return self._insertions

    @insertions.setter
    def insertions(self, insertions):
        """Sets the insertions of this TestCycle.


        :param insertions: The insertions of this TestCycle.  # noqa: E501
        :type insertions: int
        """

        self._insertions = insertions

    @property
    def deletions(self):
        """Gets the deletions of this TestCycle.  # noqa: E501


        :return: The deletions of this TestCycle.  # noqa: E501
        :rtype: int
        """
        return self._deletions

    @deletions.setter
    def deletions(self, deletions):
        """Sets the deletions of this TestCycle.


        :param deletions: The deletions of this TestCycle.  # noqa: E501
        :type deletions: int
        """

        self._deletions = deletions

    def to_dict(self, serialize=False):
        """Returns the model properties as a dict"""
        result = {}

        def convert(x):
            if hasattr(x, "to_dict"):
                args = getfullargspec(x.to_dict).args
                if len(args) == 1:
                    return x.to_dict()
                else:
                    return x.to_dict(serialize)
            else:
                return x

        for attr, _ in six.iteritems(self.openapi_types):
            value = getattr(self, attr)
            attr = self.attribute_map.get(attr, attr) if serialize else attr
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: convert(x),
                    value
                ))
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], convert(item[1])),
                    value.items()
                ))
            else:
                result[attr] = convert(value)

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, TestCycle):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, TestCycle):
            return True

        return self.to_dict() != other.to_dict()
