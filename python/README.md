# Comet API Clients #
## Python Example ##

You need Conda installed. 

First, let's create the conda environment and activate it. In the python/ directory, run:
```console
(base) foo@bar:~/comet-clients/python$ conda env create -f conda-env.yml
(cometclient) foo@bar:~/comet-clients/python$ conda activate cometclient
```
Then, to execute the example, run the following command in the python directory:
### TODO: replace cmd
```console
(cometclient) foo@bar:~/comet-clients/python$ python api_usage_example_dl4j.py
```
The program should generate a CSV file with APFD scores.
