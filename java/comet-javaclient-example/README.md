# Comet API Clients #
## Java Example ##

You need Java 11+ and Maven installed.

First, the client must be packaged and put in your local repository. Go to java/comet-javaclient and run:
```console
foo@bar:~/comet-clients/java/comet-javaclient$ mvn clean install
```
Second, copy the dataset to the resource folder of the comet-javaclient-example project:
```console
foo@bar:~/comet-clients$ cp dataset/deeplearning4j-api.csv java/comet-javaclient-example/src/main/resources/com/smartesting/comet
```
Third, go to java/comet-javaclient-example and run:
```console
foo@bar:~/comet-clients/java/comet-javaclient-example$ mvn compile exec:java -Dexec.mainClass="com.smartesting.comet.CometJavaClientExample"
```
The program should output a CSV file containing the APFD scores of Comet and the prioritization times.
