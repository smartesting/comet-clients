package com.smartesting.comet;

import com.smartesting.comet.api.PrioritizationsApi;
import com.smartesting.comet.api.ProjectsApi;
import com.smartesting.comet.api.TestCyclesApi;
import com.smartesting.comet.api.TestsApi;
import com.smartesting.comet.auth.ApiKeyAuth;
import com.smartesting.comet.model.*;
import tech.tablesaw.api.DoubleColumn;
import tech.tablesaw.api.IntColumn;
import tech.tablesaw.api.Row;
import tech.tablesaw.api.Table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static com.smartesting.comet.Configuration.getDefaultApiClient;
import static com.smartesting.comet.Env.getEnv;
import static com.smartesting.comet.model.Feature.TypeEnum;
import static java.util.stream.IntStream.range;

/**
 * Hello world!
 */
public class CometJavaClientExample {
    private final static String prjName = "deeplearning4j-java";

    public static void main(String[] args) throws Exception {
        final Table dl4jData = Table.read().csv(
                CometJavaClientExample.class.getResourceAsStream("deeplearning4j-api.csv"));

        int currentCycle = 1;
        IntColumn allCycles = (IntColumn) dl4jData.column("job_id").unique();
        int nbrCycles = allCycles.size();

        final ApiClient client = initAPI();
        client.setReadTimeout(500000); // 5 minutes
        final ProjectsApi projectsApi = new ProjectsApi(client);

        try {
            projectsApi.deleteProject(prjName);
            System.out.println("[INFO] Project " + prjName + " already exists. Deleting it.");
        } catch (ApiException ignored) {

        }

        try {
            final Project project = new Project().name(prjName);
            project.features(newArrayList(
                    new Feature().name("duration").type(TypeEnum.FLOAT).defaultValue("0.0"),
                    new Feature().name("class_changed").type(TypeEnum.BOOL).defaultValue("false"),
                    new Feature().name("test_changed").type(TypeEnum.BOOL).defaultValue("false")
            ));
            projectsApi.createProject(project);
            System.out.println("[INFO] Creating project " + prjName);
        } catch (ApiException ae) {
            System.err.println("Project " + prjName + " already exists.");
            System.exit(1);
        }

        ArrayList<Integer> processedTestCycles = new ArrayList<>();
        ArrayList<Double> optimalApfds = new ArrayList<>();
        ArrayList<Double> initialApfds = new ArrayList<>();
        ArrayList<Double> cometApfds = new ArrayList<>();
        ArrayList<Double> execTimes = new ArrayList<>();

        // Simulating multiple CI cycles
        for (int testcycleId : allCycles.asSet().stream().limit(100).collect(Collectors.toList())) {
            Table testCases = dl4jData.where(dl4jData.intColumn("job_id").isEqualTo(testcycleId));
            final List<Test> tests = newArrayList();
            int nbrTestCases = testCases.rowCount();
            int nbrFailedTestCases = testCases.where(testCases.intColumn("verdict").isGreaterThan(0)).rowCount();
            // The first 20 test cycles are considered historical data and are pushed as is in DB
            if (currentCycle < 30) {
                System.out.println("[INFO] No prediction. Database storage only.");
                for (Row testcase : testCases) {
                    Test test = new Test();
                    test.put("id", String.valueOf(testcase.getString("testcase_name")));
                    test.put("fail", testcase.getInt("verdict") > 0);
                    test.put("class_changed", testcase.getInt("class_changed") == 1);
                    test.put("test_changed", testcase.getInt("test_changed") == 1);
                    test.put("duration", (float) testcase.getDouble("duration"));
                    tests.add(test);
                    System.out.println("[INFO] Adding test " + tests.size() + "/" + nbrTestCases + ": "
                            + testcase.getString("testcase_name"));
                }
                final TestCycle cycle = new TestCycle()
                        .id(String.valueOf(testcycleId))
                        .tests(tests);
                final TestCyclesApi cyclesApi = new TestCyclesApi(client);
                cyclesApi.addTestCycle(prjName, cycle, false);
                System.out.println("[INFO] Creating Test Cycle " + currentCycle + "/" + nbrCycles);
                /* For each subsequent cycles:
                 *    1. test data is sent to Comet
                 *    2. Comet sends a prioritized order
                 *    3. test execution results are sent to comet
                 */
            } else {
                System.out.println("[INFO] Submitting test cases planned for execution.");
                for (Row testcase : testCases) {
                    Test test = new Test();
                    test.put("id", String.valueOf(testcase.getString("testcase_name")));
                    test.put("class_changed", (testcase.getInt("class_changed") == 1));
                    test.put("test_changed", (testcase.getInt("test_changed") == 1));
                    tests.add(test);
                    System.out.println("[INFO] Adding test " + tests.size() + "/" + nbrTestCases + ":"
                            + testcase.getString("testcase_name"));
                }
                final TestCycle cycle = new TestCycle()
                        .id(String.valueOf(testcycleId))
                        .tests(tests);
                final TestCyclesApi cyclesApi = new TestCyclesApi(client);
                cyclesApi.addTestCycle(prjName, cycle, false);
                System.out.println("[INFO] Creating Test Cycle " + currentCycle + "/" + nbrCycles);

                // Because we know in advance which test cycles do not contain failed test cases
                // It is useless to call comet on these
                if (nbrTestCases > 6 && nbrFailedTestCases > 0) {
                    System.out.println("[INFO] Request prioritization");
                    long start = System.nanoTime();
                    final PrioritizationsApi prioritizationsApi = new PrioritizationsApi(client);
                    final Prioritization prioritization = prioritizationsApi.prioritize(prjName, String.valueOf(testcycleId));
                    final List<String> prioTestsList = prioritization.getTests();
                    //prioritization.getTests().forEach(tc -> prioTestsList.add(Integer.valueOf(tc))); //prioritization.getTests().stream().mapToInt(Integer::parseInt).collect(Collectors.toList());
                    long end = System.nanoTime();

                    processedTestCycles.add(currentCycle);
                    optimalApfds.add(computeOptimalAPFD(testCases.rowCount(), nbrFailedTestCases));
                    initialApfds.add(compute_APFD(testCases));
                    cometApfds.add(compute_APFD(testCases, prioTestsList));
                    execTimes.add((double) (end - start) / 1000000000);

                    System.out.println("[INFO] " + nbrFailedTestCases + "/" + nbrTestCases + " test cases failed.");
                    System.out.println("[INFO] Prioritization took " + String.format("%,.2f", execTimes.get(execTimes.size() - 1)) + " seconds.");
                    System.out.println("[INFO] APFD of Optimal order, initial order, prioritized order: " +
                            optimalApfds.get(optimalApfds.size() - 1) + "," +
                            initialApfds.get(optimalApfds.size() - 1) + "," +
                            cometApfds.get(optimalApfds.size() - 1));
                }

                System.out.println("[INFO] Update test suite with execution results");
                final TestsApi testsApi = new TestsApi(client);
                List<TestVerdict> verdicts = new ArrayList<>();
                for (Row testcase : testCases) {
                    TestVerdict verdict = new TestVerdict();
                    verdict.put("id", testcase.getString("testcase_name"));
                    verdict.put("fail", testcase.getInt("verdict") > 0);
                    verdict.put("duration", (float) testcase.getDouble("duration"));
                    verdicts.add(verdict);
                }
                testsApi.updateSuite(prjName, String.valueOf(testcycleId), verdicts);
            }
            currentCycle++;
        }

        Table apfdScores = Table.create("APFD Scores").addColumns(
                IntColumn.create("cycle_id", processedTestCycles.stream().mapToInt(Integer::valueOf).toArray()),
                DoubleColumn.create("optimal_apfds", optimalApfds.stream().mapToDouble(Double::valueOf).toArray()),
                DoubleColumn.create("initial_apfds", initialApfds.stream().mapToDouble(Double::valueOf).toArray()),
                DoubleColumn.create("comet_apfds", cometApfds.stream().mapToDouble(Double::valueOf).toArray()),
                DoubleColumn.create("exec_times", execTimes.stream().mapToDouble(Double::valueOf).toArray()));

        apfdScores.write().csv("dl4j-cometapi-results.csv");
        System.exit(0);
    }

    private static ApiClient initAPI() {
        final ApiClient client = getDefaultApiClient();
        client.setBasePath(getEnv("COMET_URL"));
        final ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) client.getAuthentication("ApiKeyAuth");
        ApiKeyAuth.setApiKey(getEnv("COMET_API_KEY"));
        return client;
    }

    private static double compute_APFD(Table testcases) {
        return compute_APFD(testcases, Collections.EMPTY_LIST);
    }

    private static double compute_APFD(Table testcases, List<String> prioSuite) {
        float N = testcases.rowCount();
        float M = testcases.where(
                testcases.intColumn("verdict").isEqualTo(1)
        ).rowCount();
        if (prioSuite.isEmpty()) {
            prioSuite = testcases.stringColumn("testcase_name").asList();
        }
        double apfd = -1;
        if (N > 0 && M > 0) {
            apfd = 1 - (sumRanks(testcases, prioSuite) / (N * M)) + (1 / (2.f * N));
        }
        return apfd;
    }

    private static double sumRanks(Table testcases, List<String> prioSuite) {
        double sumRanks = 0;
        int i = 1;
        for (String tc : prioSuite) {
            sumRanks += testcases.where(testcases.stringColumn("testcase_name")
                    .isEqualTo(tc)).intColumn("verdict").getInt(0) * i;
            i++;
        }
        return sumRanks;
    }

    private static double computeOptimalAPFD(int testSuiteSize, int nbrFailedTc) {
        if (testSuiteSize > 0 && nbrFailedTc > 0) {
            return 1 - ((double) range(1, nbrFailedTc + 1).sum() / (testSuiteSize * nbrFailedTc)) + (1 / (2.f * testSuiteSize));
        } else {
            return -1;
        }
    }
}
