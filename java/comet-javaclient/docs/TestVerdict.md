

# TestVerdict


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**fail** | **Boolean** |  |  [optional]
**duration** | **Float** |  |  [optional]



