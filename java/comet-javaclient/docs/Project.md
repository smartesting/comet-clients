

# Project


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**testCycles** | [**List&lt;TestCycle&gt;**](TestCycle.md) |  |  [optional]
**features** | [**List&lt;Feature&gt;**](Feature.md) |  |  [optional]



