# TestsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addTest**](TestsApi.md#addTest) | **POST** /projects/{name}/testCycles/{id}/test | Add a test to a test cycle
[**replaceSuite**](TestsApi.md#replaceSuite) | **PUT** /projects/{name}/testCycles/{id}/suite | Set the test suite of a test cycle
[**replaceTest**](TestsApi.md#replaceTest) | **PUT** /projects/{name}/testCycles/{id}/test | Set a test
[**updateSuite**](TestsApi.md#updateSuite) | **PATCH** /projects/{name}/testCycles/{id}/suite | Update a test suite
[**updateTest**](TestsApi.md#updateTest) | **PATCH** /projects/{name}/testCycles/{id}/test | Update a test


<a name="addTest"></a>
# **addTest**
> Test addTest(name, id, test)

Add a test to a test cycle

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.TestsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    TestsApi apiInstance = new TestsApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    String id = "id_example"; // String | ID of the test cycle containing the suite to add test to
    Map<String, Object> test = null; // Map<String, Object> | Test to add
    try {
      Test result = apiInstance.addTest(name, id, test);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TestsApi#addTest");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |
 **id** | **String**| ID of the test cycle containing the suite to add test to |
 **test** | [**Map&lt;String, Object&gt;**](Object.md)| Test to add |

### Return type

[**Test**](Test.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The added test is returned |  * x-next - A link to the next page of responses <br>  |
**404** | The project or the test cycle do not exist |  -  |
**409** | Test already exist |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

<a name="replaceSuite"></a>
# **replaceSuite**
> List&lt;Test&gt; replaceSuite(name, id, tests)

Set the test suite of a test cycle

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.TestsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    TestsApi apiInstance = new TestsApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    String id = "id_example"; // String | ID of the test cycle to add suite to
    List<Test> tests = Arrays.asList(); // List<Test> | Suite to add
    try {
      List<Test> result = apiInstance.replaceSuite(name, id, tests);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TestsApi#replaceSuite");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |
 **id** | **String**| ID of the test cycle to add suite to |
 **tests** | [**List&lt;Test&gt;**](Test.md)| Suite to add |

### Return type

[**List&lt;Test&gt;**](Test.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The modified test cycle is returned |  * x-next - A link to the next page of responses <br>  |
**404** | The project or the test cycle do not exist |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

<a name="replaceTest"></a>
# **replaceTest**
> TestCycle replaceTest(name, id, test)

Set a test

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.TestsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    TestsApi apiInstance = new TestsApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    String id = "id_example"; // String | ID of the test cycle to add a test to
    Map<String, Object> test = null; // Map<String, Object> | Test to add
    try {
      TestCycle result = apiInstance.replaceTest(name, id, test);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TestsApi#replaceTest");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |
 **id** | **String**| ID of the test cycle to add a test to |
 **test** | [**Map&lt;String, Object&gt;**](Object.md)| Test to add |

### Return type

[**TestCycle**](TestCycle.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The modified test cycle is returned |  * x-next - A link to the next page of responses <br>  |
**404** | The project or the test cycle do not exist |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

<a name="updateSuite"></a>
# **updateSuite**
> List&lt;TestVerdict&gt; updateSuite(name, id, verdicts)

Update a test suite

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.TestsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    TestsApi apiInstance = new TestsApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    String id = "id_example"; // String | ID of the test cycle to add suite to
    List<TestVerdict> verdicts = Arrays.asList(); // List<TestVerdict> | Changes to apply to the test suite
    try {
      List<TestVerdict> result = apiInstance.updateSuite(name, id, verdicts);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TestsApi#updateSuite");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |
 **id** | **String**| ID of the test cycle to add suite to |
 **verdicts** | [**List&lt;TestVerdict&gt;**](TestVerdict.md)| Changes to apply to the test suite |

### Return type

[**List&lt;TestVerdict&gt;**](TestVerdict.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The modified test cycle is returned |  * x-next - A link to the next page of responses <br>  |
**404** | The project or the test cycle do not exist |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

<a name="updateTest"></a>
# **updateTest**
> TestVerdict updateTest(name, id, verdict)

Update a test

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.TestsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    TestsApi apiInstance = new TestsApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    String id = "id_example"; // String | ID of the targeted test cycle
    Map<String, Object> verdict = null; // Map<String, Object> | Changes to apply to the test
    try {
      TestVerdict result = apiInstance.updateTest(name, id, verdict);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TestsApi#updateTest");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |
 **id** | **String**| ID of the targeted test cycle |
 **verdict** | [**Map&lt;String, Object&gt;**](Object.md)| Changes to apply to the test |

### Return type

[**TestVerdict**](TestVerdict.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The modified test cycle is returned |  * x-next - A link to the next page of responses <br>  |
**404** | The project or the test cycle do not exist |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

