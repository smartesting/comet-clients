

# TestCycle


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**tests** | **List&lt;Test&gt;** |  |  [optional]
**sinceLast** | **Integer** |  |  [optional]
**filesChanged** | **Integer** |  |  [optional]
**insertions** | **Integer** |  |  [optional]
**deletions** | **Integer** |  |  [optional]



