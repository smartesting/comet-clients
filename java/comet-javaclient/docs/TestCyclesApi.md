# TestCyclesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addTestCycle**](TestCyclesApi.md#addTestCycle) | **POST** /projects/{name}/testCycles | Add a new test cycle to a project
[**deleteTestCycle**](TestCyclesApi.md#deleteTestCycle) | **DELETE** /projects/{name}/testCycles/{id} | Remove a test cycle from a project
[**listTestCycles**](TestCyclesApi.md#listTestCycles) | **GET** /projects/{name}/testCycles | List test cycles of a project
[**showLastTestCycle**](TestCyclesApi.md#showLastTestCycle) | **GET** /projects/{name}/testCycles/last | Show the last cycle
[**showPreviousTestCycle**](TestCyclesApi.md#showPreviousTestCycle) | **GET** /projects/{name}/testCycles/{id}/previous | Show the previous cycle
[**showTestCycle**](TestCyclesApi.md#showTestCycle) | **GET** /projects/{name}/testCycles/{id} | Show a test cycle


<a name="addTestCycle"></a>
# **addTestCycle**
> TestCycle addTestCycle(name, testCycle, previousTests)

Add a new test cycle to a project

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.TestCyclesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    TestCyclesApi apiInstance = new TestCyclesApi(defaultClient);
    String name = "name_example"; // String | Name of the project to add a test cycle to
    TestCycle testCycle = new TestCycle(); // TestCycle | TestCycle to add to the specified project
    Boolean previousTests = false; // Boolean | Indicate if tests of the previous build are used in this one
    try {
      TestCycle result = apiInstance.addTestCycle(name, testCycle, previousTests);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TestCyclesApi#addTestCycle");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project to add a test cycle to |
 **testCycle** | [**TestCycle**](TestCycle.md)| TestCycle to add to the specified project |
 **previousTests** | **Boolean**| Indicate if tests of the previous build are used in this one | [optional] [default to false]

### Return type

[**TestCycle**](TestCycle.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | The test cycle is created and returned |  -  |
**404** | The project does not exist |  -  |
**409** | Cycle already exists |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

<a name="deleteTestCycle"></a>
# **deleteTestCycle**
> List&lt;TestCycle&gt; deleteTestCycle(name, id)

Remove a test cycle from a project

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.TestCyclesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    TestCyclesApi apiInstance = new TestCyclesApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    String id = "id_example"; // String | ID of the test cycle to delete
    try {
      List<TestCycle> result = apiInstance.deleteTestCycle(name, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TestCyclesApi#deleteTestCycle");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |
 **id** | **String**| ID of the test cycle to delete |

### Return type

[**List&lt;TestCycle&gt;**](TestCycle.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | The test cycle is removed from the project |  -  |
**404** | The project or the test cycle do not exist |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

<a name="listTestCycles"></a>
# **listTestCycles**
> List&lt;TestCycle&gt; listTestCycles(name)

List test cycles of a project

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.TestCyclesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    TestCyclesApi apiInstance = new TestCyclesApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    try {
      List<TestCycle> result = apiInstance.listTestCycles(name);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TestCyclesApi#listTestCycles");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |

### Return type

[**List&lt;TestCycle&gt;**](TestCycle.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The test cycle is found and returned |  -  |
**404** | The project does not exist |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

<a name="showLastTestCycle"></a>
# **showLastTestCycle**
> TestCycle showLastTestCycle(name)

Show the last cycle

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.TestCyclesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    TestCyclesApi apiInstance = new TestCyclesApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    try {
      TestCycle result = apiInstance.showLastTestCycle(name);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TestCyclesApi#showLastTestCycle");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |

### Return type

[**TestCycle**](TestCycle.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The last test cycle is found and fully returned |  * x-next - A link to the next page of responses <br>  |
**404** | The project or the test cycle do not exist |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

<a name="showPreviousTestCycle"></a>
# **showPreviousTestCycle**
> TestCycle showPreviousTestCycle(name, id)

Show the previous cycle

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.TestCyclesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    TestCyclesApi apiInstance = new TestCyclesApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    String id = "id_example"; // String | ID of the test cycle
    try {
      TestCycle result = apiInstance.showPreviousTestCycle(name, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TestCyclesApi#showPreviousTestCycle");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |
 **id** | **String**| ID of the test cycle |

### Return type

[**TestCycle**](TestCycle.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The previous test cycle is found and fully returned |  * x-next - A link to the next page of responses <br>  |
**404** | The project or the test cycle do not exist |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

<a name="showTestCycle"></a>
# **showTestCycle**
> TestCycle showTestCycle(name, id)

Show a test cycle

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.TestCyclesApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    TestCyclesApi apiInstance = new TestCyclesApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    String id = "id_example"; // String | ID of the test cycle to show
    try {
      TestCycle result = apiInstance.showTestCycle(name, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling TestCyclesApi#showTestCycle");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |
 **id** | **String**| ID of the test cycle to show |

### Return type

[**TestCycle**](TestCycle.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The test cycle is found and fully returned |  * x-next - A link to the next page of responses <br>  |
**404** | The project or the test cycle do not exist |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

