# PrioritizationsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addAndPrioritize**](PrioritizationsApi.md#addAndPrioritize) | **POST** /projects/{name}/testCycles/{id}/tcp | Add and prioritize a test suite
[**prioritize**](PrioritizationsApi.md#prioritize) | **GET** /projects/{name}/testCycles/{id}/tcp | Prioritize test suite of test cycle
[**prioritizeWithoutAdd**](PrioritizationsApi.md#prioritizeWithoutAdd) | **POST** /projects/{name}/testCycles/tcp | Prioritize a temporary test cycle


<a name="addAndPrioritize"></a>
# **addAndPrioritize**
> Prioritization addAndPrioritize(name, id, tests)

Add and prioritize a test suite

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.PrioritizationsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    PrioritizationsApi apiInstance = new PrioritizationsApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    String id = "id_example"; // String | ID of the test cycle to add suite to and prioritize
    List<Test> tests = Arrays.asList(); // List<Test> | Suite to add and prioritize
    try {
      Prioritization result = apiInstance.addAndPrioritize(name, id, tests);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PrioritizationsApi#addAndPrioritize");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |
 **id** | **String**| ID of the test cycle to add suite to and prioritize |
 **tests** | [**List&lt;Test&gt;**](Test.md)| Suite to add and prioritize |

### Return type

[**Prioritization**](Prioritization.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The list of the test ordered according an ML algorithm |  * x-next - A link to the next page of responses <br>  |
**404** | The project or the test cycle do not exist |  -  |
**409** | One or many tests already exist or not enough data to prioritize |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

<a name="prioritize"></a>
# **prioritize**
> Prioritization prioritize(name, id)

Prioritize test suite of test cycle

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.PrioritizationsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    PrioritizationsApi apiInstance = new PrioritizationsApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    String id = "id_example"; // String | ID of the test cycle to prioritize
    try {
      Prioritization result = apiInstance.prioritize(name, id);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PrioritizationsApi#prioritize");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project |
 **id** | **String**| ID of the test cycle to prioritize |

### Return type

[**Prioritization**](Prioritization.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The list of the tests ordered according an ML algorithm |  * x-next - A link to the next page of responses <br>  |
**404** | The project or the test cycle do not exist |  -  |
**409** | Not enough data to prioritize |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

<a name="prioritizeWithoutAdd"></a>
# **prioritizeWithoutAdd**
> Prioritization prioritizeWithoutAdd(name, testCycle)

Prioritize a temporary test cycle

### Example
```java
// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.PrioritizationsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    PrioritizationsApi apiInstance = new PrioritizationsApi(defaultClient);
    String name = "name_example"; // String | Name of the project to use to prioritize
    TmpTestCycle testCycle = new TmpTestCycle(); // TmpTestCycle | A test cycle (without ID) to prioritize
    try {
      Prioritization result = apiInstance.prioritizeWithoutAdd(name, testCycle);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PrioritizationsApi#prioritizeWithoutAdd");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of the project to use to prioritize |
 **testCycle** | [**TmpTestCycle**](TmpTestCycle.md)| A test cycle (without ID) to prioritize |

### Return type

[**Prioritization**](Prioritization.md)

### Authorization

[ApiKeyAuth](../README.md#ApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | The list of the test ordered according an ML algorithm |  -  |
**404** | The project does not exist |  -  |
**409** | Not enough data |  -  |
**401** | Unauthorized |  -  |
**0** | Unexpected error |  -  |

