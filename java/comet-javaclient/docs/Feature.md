

# Feature


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**type** | [**TypeEnum**](#TypeEnum) |  | 
**defaultValue** | **String** |  | 



## Enum: TypeEnum

Name | Value
---- | -----
FLOAT | &quot;float&quot;
INT | &quot;int&quot;
BOOL | &quot;bool&quot;



