

# TmpTestCycle


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tests** | **List&lt;Test&gt;** |  |  [optional]
**filesChanged** | **Integer** |  |  [optional]
**insertions** | **Integer** |  |  [optional]
**deletions** | **Integer** |  |  [optional]
**sinceLast** | **Integer** |  |  [optional]



