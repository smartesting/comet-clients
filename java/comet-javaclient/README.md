# javaclient

Comet API
- API version: 2.0.0
  - Build date: 2022-06-23T11:05:13.466967100+02:00[Europe/Paris]

No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)


*Automatically generated by the [OpenAPI Generator](https://openapi-generator.tech)*


## Requirements

Building the API client library requires:
1. Java 1.7+
2. Maven/Gradle

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn clean install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn clean deploy
```

Refer to the [OSSRH Guide](http://central.sonatype.org/pages/ossrh-guide.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
  <groupId>com.smartesting.comet</groupId>
  <artifactId>javaclient</artifactId>
  <version>2.0.0</version>
  <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.smartesting.comet:javaclient:2.0.0"
```

### Others

At first generate the JAR by executing:

```shell
mvn clean package
```

Then manually install the following JARs:

* `target/javaclient-2.0.0.jar`
* `target/lib/*.jar`

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

// Import classes:
import com.smartesting.comet.ApiClient;
import com.smartesting.comet.ApiException;
import com.smartesting.comet.Configuration;
import com.smartesting.comet.auth.*;
import com.smartesting.comet.models.*;
import com.smartesting.comet.api.PrioritizationsApi;

public class Example {
  public static void main(String[] args) {
    ApiClient defaultClient = Configuration.getDefaultApiClient();
    defaultClient.setBasePath("http://localhost");
    
    // Configure API key authorization: ApiKeyAuth
    ApiKeyAuth ApiKeyAuth = (ApiKeyAuth) defaultClient.getAuthentication("ApiKeyAuth");
    ApiKeyAuth.setApiKey("YOUR API KEY");
    // Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
    //ApiKeyAuth.setApiKeyPrefix("Token");

    PrioritizationsApi apiInstance = new PrioritizationsApi(defaultClient);
    String name = "name_example"; // String | Name of the project
    String id = "id_example"; // String | ID of the test cycle to add suite to and prioritize
    List<Test> tests = Arrays.asList(); // List<Test> | Suite to add and prioritize
    try {
      Prioritization result = apiInstance.addAndPrioritize(name, id, tests);
      System.out.println(result);
    } catch (ApiException e) {
      System.err.println("Exception when calling PrioritizationsApi#addAndPrioritize");
      System.err.println("Status code: " + e.getCode());
      System.err.println("Reason: " + e.getResponseBody());
      System.err.println("Response headers: " + e.getResponseHeaders());
      e.printStackTrace();
    }
  }
}

```

## Documentation for API Endpoints

All URIs are relative to *http://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*PrioritizationsApi* | [**addAndPrioritize**](docs/PrioritizationsApi.md#addAndPrioritize) | **POST** /projects/{name}/testCycles/{id}/tcp | Add and prioritize a test suite
*PrioritizationsApi* | [**prioritize**](docs/PrioritizationsApi.md#prioritize) | **GET** /projects/{name}/testCycles/{id}/tcp | Prioritize test suite of test cycle
*PrioritizationsApi* | [**prioritizeWithoutAdd**](docs/PrioritizationsApi.md#prioritizeWithoutAdd) | **POST** /projects/{name}/testCycles/tcp | Prioritize a temporary test cycle
*ProjectsApi* | [**createProject**](docs/ProjectsApi.md#createProject) | **POST** /projects | Create a new project
*ProjectsApi* | [**deleteProject**](docs/ProjectsApi.md#deleteProject) | **DELETE** /projects/{name} | Delete project
*ProjectsApi* | [**showProject**](docs/ProjectsApi.md#showProject) | **GET** /projects/{name} | Show an project
*TestCyclesApi* | [**addTestCycle**](docs/TestCyclesApi.md#addTestCycle) | **POST** /projects/{name}/testCycles | Add a new test cycle to a project
*TestCyclesApi* | [**deleteTestCycle**](docs/TestCyclesApi.md#deleteTestCycle) | **DELETE** /projects/{name}/testCycles/{id} | Remove a test cycle from a project
*TestCyclesApi* | [**listTestCycles**](docs/TestCyclesApi.md#listTestCycles) | **GET** /projects/{name}/testCycles | List test cycles of a project
*TestCyclesApi* | [**showLastTestCycle**](docs/TestCyclesApi.md#showLastTestCycle) | **GET** /projects/{name}/testCycles/last | Show the last cycle
*TestCyclesApi* | [**showPreviousTestCycle**](docs/TestCyclesApi.md#showPreviousTestCycle) | **GET** /projects/{name}/testCycles/{id}/previous | Show the previous cycle
*TestCyclesApi* | [**showTestCycle**](docs/TestCyclesApi.md#showTestCycle) | **GET** /projects/{name}/testCycles/{id} | Show a test cycle
*TestsApi* | [**addTest**](docs/TestsApi.md#addTest) | **POST** /projects/{name}/testCycles/{id}/test | Add a test to a test cycle
*TestsApi* | [**replaceSuite**](docs/TestsApi.md#replaceSuite) | **PUT** /projects/{name}/testCycles/{id}/suite | Set the test suite of a test cycle
*TestsApi* | [**replaceTest**](docs/TestsApi.md#replaceTest) | **PUT** /projects/{name}/testCycles/{id}/test | Set a test
*TestsApi* | [**updateSuite**](docs/TestsApi.md#updateSuite) | **PATCH** /projects/{name}/testCycles/{id}/suite | Update a test suite
*TestsApi* | [**updateTest**](docs/TestsApi.md#updateTest) | **PATCH** /projects/{name}/testCycles/{id}/test | Update a test


## Documentation for Models

 - [Error](docs/Error.md)
 - [Feature](docs/Feature.md)
 - [Prioritization](docs/Prioritization.md)
 - [Project](docs/Project.md)
 - [Test](docs/Test.md)
 - [TestCycle](docs/TestCycle.md)
 - [TestVerdict](docs/TestVerdict.md)
 - [TmpTestCycle](docs/TmpTestCycle.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### ApiKeyAuth

- **Type**: API key
- **API key parameter name**: X-API-KEY
- **Location**: HTTP header


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



